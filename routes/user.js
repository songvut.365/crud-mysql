const router = require('express').Router();
const bcrypt = require('bcrypt');

const pool = require('../config/database')


//Get users
router.get('/', async (req, res) => {
  let query = "SELECT * FROM user"

  await pool.getConnection((err, connection) => {
    if (err) throw err;

    connection.query(query, (err, rows) => {
      connection.release();

      if(err) throw err;
      res.send(rows);
    });
  });
});


//Get user by id
router.get('/:id', async (req, res) => {
  let query = 'SELECT * FROM user WHERE id =' + req.params.id;

  await pool.getConnection((err, connection) => {
    if (err) throw err;

    //connect sucess
    connection.query(query ,(err, rows) => {
        //return connection to pool
        connection.release();
        
        if (err) throw err;
        res.send(rows[0])
      }
    )
  });
});


//Update
router.patch('/:id', async (req, res) => {
  const id = req.params.id;
  const first_name = req.body.first_name;
  const last_name = req.body.last_name;

  let query = "UPDATE user SET first_name = '"+first_name+"', last_name = '"+last_name+"' WHERE id = '"+id+"'";

  await pool.getConnection((err, connection) => {
    if (err) throw err;

    connection.query(query, (err, response) => {
      connection.release();

      if (err) throw err;
      res.json({"message": "update successfully"})
    });
  });
});



//Delete
router.delete('/:id', async (req, res) => {
  const query = "DELETE FROM user WHERE id = '"+req.params.id +"'";
  
  await pool.getConnection((err, connection) => {
    if (err) throw err;

    connection.query(query, (err, response) => {
      connection.release();

      if (err) throw err;
      res.json({"message": "delete successfully"})
    });
  });
});


module.exports = router;