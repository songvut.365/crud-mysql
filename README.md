# CRUD Application with MySQL

## Project setup

### Database setup (.env)
```
DB_HOST=localhost
DB_USER=root
DB_PASSWORD=password
DB_DATABASE=EXAMPLE
```


### Install package
```
npm install
```

### Complies
```
node index.js
```

or

(nodemon required)

```
nodemon index.js
```

### Customize configuration
See [Configuration Reference](http://expressjs.com/).
