require("dotenv").config();
const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const { nanoid } = require('nanoid');

const users = require("./routes/user");
const pool = require('./config/database');

//Use express
const app = express();
app.use(express());

//bodyParser setting
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//Index
app.get('/', (req, res) => {
  res.send('Welcome!')
});

//Login
app.post('/login', async (req, res) => {
  let email = '';
  let password = '';
  let query = "SELECT * FROM user WHERE email = '" + req.body.email + "'";

  await pool.getConnection((err, connection) => {
    if (err) throw err;

    //connect sucess
    connection.query(query ,(err, rows) => {
        //return connection to pool
        connection.release();
        
        if (err) throw err;
        //Get password from data
        email = rows[0].email
        password = rows[0].password

        //Compare password and password from database
        bcrypt.compare(req.body.password, password, (err, result) => {
          if (result) {
            res.json({"message": "login successfully"})
          }
          else {
            res.json({"message": "login failed"})
          }
        });
      }
    );
  });
});

//Register
app.post('/register', async (req, res) => {
  //generate id
  const id = nanoid();

  const first_name = req.body.first_name;
  const last_name = req.body.last_name;
  const email = req.body.email;
  const password = req.body.password;
  
  //encrypt password
  var encryptedPassword = await bcrypt.hash(password, 10);

  const value = "'"+id+"', '"+first_name+"', '"+last_name+"', '"+email+"', '"+encryptedPassword+"'";

  let query = "INSERT INTO user (id, first_name, last_name, email, password) VALUES ("+value+")";
  await pool.getConnection((err, connection) => {
    if (err) throw err;

    connection.query(query, (err, response) => {
      connection.release();

      if (err) throw err
      res.json({"message": "register successfully"})
    });
  });
});


//Router /users
app.use('/users', users);

module.exports = app;